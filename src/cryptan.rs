extern crate rand;

use math;
use block::{gen_rand_key, encrypt_aes_128_ecb_no_pad, encrypt_aes_128_cbc};
#[allow(unused_imports)]
use conversions::{byte_slice_to_hex, bytes_to_hex};
#[allow(unused_imports)]
use cryptan::rand::prelude::*;

pub fn aes_ecb_detect(ct: &str) -> Vec<u8> {
    let mut max_fit = 0.0;
    let mut likeliest_bytes = Vec::new();
    for line in ct.lines() {
        let bytes = hex::decode(&line).expect("Unable to decode a line.");
        let fitness = aes_ecb_fit(&bytes);
        if fitness > max_fit {
            max_fit = fitness;
            likeliest_bytes = bytes;
        }
    }
    likeliest_bytes
}

pub fn aes_ecb_attack(ct: &[u8]) -> Option<Vec<u8>> {
    use std::collections::HashMap;

    let key = gen_rand_key();
    let len = detect_cipher_block_size();
    //let mode = oracles::aes_ecb_cbc_detection_oracle(ct);
    //if mode == oracles::AESMode::CBC {
    //    return None;
    //}

    let mut pt = Vec::new();
    let mut ct_vec = ct.iter().cloned().collect::<Vec<u8>>();
    let mut input_block = Vec::new();
    for _ in 0..len -1 {
        input_block.push(1); 
    }

    while !ct_vec.is_empty() {
        let mut map = HashMap::new();
        let temp_ct = oracles::aes_encryption_oracle_ecb_consistent(
            &ct_vec[..], &input_block[..], &key[..]);
        let original_hex = byte_slice_to_hex(&temp_ct[0..len]);
        map.insert(original_hex, true);

        for i in 0..=255 {
            input_block.push(i);
            //println!("Testing: {:?}", input_block);
            let result = oracles::aes_encryption_oracle_ecb_consistent(
                &ct_vec[..], &input_block[..], &key[..]);
            
            let res_str = byte_slice_to_hex(&result[0..len]);
            if map.contains_key(&res_str) {
                let byte = input_block.pop().unwrap();
                pt.push(byte);
                ct_vec.remove(0);
                continue;
            }
            input_block.pop();
        }
    }
    Some(pt)    
}

pub fn ecb_cut_and_paste(key: &Vec<u8>) -> String {
    use util::{encode_encrypt, decrypt_decode};
    let input1 = "nukie@seh.com";
    let mut ct1 = encode_encrypt(&input1, key).expect("Invalid email");
    //println!("ct1: {}", bytes_to_hex(&ct1));

    let mut ct2;
    {
        let mut input2 = String::from("ppppppppppadmin");
        for _ in 0..11 {
            input2.push(0xB as char);
        }
        ct2 = encode_encrypt(&input2.as_str(), key).expect("Invalid email 2");
    }
    //println!("ct2: {}", bytes_to_hex(&ct2));
    let mut crafted = Vec::new();
    crafted.append(
        &mut ct1[0..32]
            .iter()
            .cloned()
            .collect::<Vec<u8>>()
    );

    crafted.append(
        &mut ct2[16..32]
            .iter()
            .cloned()
            .collect::<Vec<u8>>()
    );
    //println!("crafted: {}", bytes_to_hex(&crafted));

    let decoded = decrypt_decode(&crafted[..], key).expect("Unable to decrypt!");
    decoded
}

fn aes_ecb_fit(bytes: &[u8]) -> f32 {
    let mut reps = 0;
    for b in [4, 8, 12, 16].iter() {
        reps += math::count_repeating_blocks(bytes, *b);
    }
    reps as f32
}

fn detect_cipher_block_size() -> usize {
    let mut dummy = Vec::new();
    dummy.push(1);
    let key = gen_rand_key();
    let init = oracles::aes_encryption_oracle_ecb_consistent(&dummy[..], &[], &key[..]).len();
    for _ in 0.. {
        dummy.push(1);
        let len = oracles::aes_encryption_oracle_ecb_consistent(&dummy[..], &[], &key[..]).len();
        if len > init {
            return init;
        }
    }
    init
}

pub fn cut_and_paste() {
    
}

pub mod oracles {
    use super::*;

    #[derive(Debug, Eq, PartialEq)]
    pub enum AESMode {
        ECB,
        CBC,
    }

    pub fn encryption_oracle_aes_ecb(pt: &[u8], key: &[u8]) -> Vec<u8> {
        encrypt_aes_128_ecb_no_pad(pt, key, None)
    }

    pub fn encryption_oracle_aes_cbc(pt: &[u8], key: &[u8]) -> Vec<u8> {
        let iv = gen_rand_key();
        encrypt_aes_128_cbc(pt, &key[..], &iv)
    }

    pub fn aes_encryption_oracle_ecb_consistent(pt: &[u8], known_bytes: &[u8], key: &[u8]) -> Vec<u8> {
        let mut new_pt = known_bytes.iter().cloned().collect::<Vec<u8>>();
        new_pt.append(&mut pt.iter().cloned().collect::<Vec<u8>>());
        encryption_oracle_aes_ecb(&new_pt[..], key)
    }

    pub fn aes_encryption_oracle_ecb_cbc(pt: &[u8]) -> Vec<u8> {
        let mut rng = rand::thread_rng();
        let fst_count = rng.gen_range(5, 11);
        let snd_count = rng.gen_range(5, 11);
        
        let mut new_pt = Vec::new();
        for _ in 0..fst_count {
            new_pt.push(fst_count as u8);
        }
        {
            let mut pt = pt.iter().cloned().collect::<Vec<u8>>();
            new_pt.append(&mut pt);
        }
        for _ in 0..snd_count {
            new_pt.push(snd_count as u8);
        }

        let key = gen_rand_key();
        if rand::random() {
            encryption_oracle_aes_ecb(&new_pt[..], &key[..])
        } else {
            encryption_oracle_aes_cbc(&new_pt[..], &key[..])
        }
    }

    pub fn aes_ecb_cbc_detection_oracle(ct: &[u8]) -> AESMode {
        if aes_ecb_fit(ct) != 0.0 {
            AESMode::ECB
        } else {
            AESMode::CBC
        }
    }
}
