extern crate openssl;

use conversions::openssl::bn::{BigNum};

pub fn hex_str_to_b64(src: &str) -> String {
    let num = BigNum::from_hex_str(&src).unwrap();
    return base64::encode(&num.to_vec());
}

pub fn bytes_to_hex(src: &Vec<u8>) -> String {
    hex::encode(src)
}

pub fn byte_slice_to_hex(src: &[u8]) -> String {
    hex::encode(src)
}

pub fn bytes_to_string(src: &Vec<u8>) -> String {
    let result: String = src.iter().map(|x| *x as char).collect();
    result
}

pub fn str_to_bytes(src: &str) -> Vec<u8> {
    src.to_owned().into_bytes()
}

pub fn copy_inplace(dest: &mut [u8], src: &[u8]) -> Option<()> {
    if dest.len() != src.len() {
        return None;
    }
    for (d, s) in dest.into_iter().zip(src.into_iter()) {
        *d = *s;
    }
    Some(())
}