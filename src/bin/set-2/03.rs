extern crate hex;
extern crate lib;

use lib::cryptan::oracles::{aes_encryption_oracle_ecb_cbc, aes_ecb_cbc_detection_oracle};

fn main() {
    println!("Running 02-03: An ECB/CBC detection oracle\n");
    let plaintext = b"XXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDDXXXXDDDD";

    for _ in 0..128 {
        let ct = aes_encryption_oracle_ecb_cbc(plaintext);
        let mode = aes_ecb_cbc_detection_oracle(&ct[..]);
        println!("DETECTED MODE: {:?}", mode);
    }
}