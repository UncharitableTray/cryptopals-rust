extern crate hex;
extern crate lib;

use lib::*;

fn main() {
    println!("Running 01-06: Break repeating-key XOR\n");

    let ciphertext = util::read_decode_b64("input_files/01/01-06.txt");
    //println!("{:?}\n", conversions::bytes_to_string(&ciphertext));
    let key = xor::break_repeating_key_xor(&ciphertext[..]);
    println!("Candidate key: {}\n", conversions::bytes_to_string(&key));
    let plaintext = xor::xor_repeating_key(&ciphertext[..], &key[..]);
    let plaintext = conversions::bytes_to_string(&plaintext);
    println!("{}", plaintext);
}