extern crate hex;
extern crate lib;

use lib::xor::{xor_bytes_vs_one, brute_xor_single};
use lib::math::ascii_fitness;

fn main() {
    println!("Running 01-03: Single-byte XOR cipher\n");
    let ctext = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    let cbytes = hex::decode(ctext).unwrap();
    //let cbytes = "ETAOIN SHRDLU".to_owned().into_bytes();
    let cypher = brute_xor_single(&cbytes);
    let best = xor_bytes_vs_one(&cbytes, cypher);
    let best_fit = ascii_fitness(&best[..]);
    let best: String = best.iter().map(|x| *x as char).collect();
    println!("Closest solution is: \"{}\" with fitness {}", best, best_fit);
}
