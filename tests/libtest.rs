extern crate lib;

use lib::conversions;
use lib::block;

#[test]
pub fn test_copy_inplace() {
    let mut dest: [u8; 5] = [1, 2, 3, 4, 5];
    let src: [u8; 5] = [5, 5, 5, 5, 5];
    let src2: [u8; 4] = [5, 4, 3, 2];

    let res1 = conversions::copy_inplace(&mut dest, &src);
    assert_eq!(res1, Some(()));
    assert_eq!(dest, src);

    let res2 = conversions::copy_inplace(&mut dest, &src2);
    assert_eq!(res2, None);
}

#[test]
pub fn test_pkcs7_padding() {
    let block = "YELLOW SUBMARINE";
    let bytes = block.as_bytes();
    let block_len = 20;
    let padded = block::pad_block_pkcs7(&bytes, block_len).unwrap();
    assert_eq!(padded, "YELLOW SUBMARINE\x04\x04\x04\x04".as_bytes());
}

#[test]
pub fn test_pkcs7_validation() {
    let valid = b"ICE ICE BABY\x04\x04\x04\x04";
    let expected = "ICE ICE BABY";
    let validated = block::validate_pkcs7(valid).unwrap();
    assert_eq!(validated, expected.as_bytes());

    let invalid = b"ICE ICE BABY\x05\x05\x05\x05";
    let validated = block::validate_pkcs7(invalid);
    assert!(validated.is_none());

    let invalid = b"ICE ICE BABY\x01\x02\x03\x04";
    let validated = block::validate_pkcs7(invalid);
    assert!(validated.is_none());
}

#[test]
pub fn test_aes_128_cbc() {
    let pt = "Ja sam mala vjeverica, i ovo je jedna jako jako dugacka poruka za male junake.".as_bytes();
    let key = "YELLOW SUBMARINE".as_bytes();
    let iv = "1337133713371337".as_bytes();

    let ct = block::encrypt_aes_128_cbc(&pt, &key, &iv);
    //println!("\nCT: {}\n", conversions::bytes_to_hex(&ct));
    let reconstructed = block::decrypt_aes_128_cbc(&ct[..], &key, &iv);

    assert_eq!(pt, &reconstructed[..]);
}

#[test]
pub fn test_kv_parse() {
    let query = "foo=bar&baz=qux&zap=zazzle";
    let expected = String::from("{\n  foo: 'bar',\n  baz: 'qux',\n  zap: 'zazzle'\n}");
    let parsed = lib::util::cookie_value_parser(&query);
    assert_eq!(expected, parsed);
}

#[test]
pub fn test_profile() {
    let mail = "foo@bar.com";
    let expected = String::from("email=foo@bar.com&uid=10&role=user");
    let parsed = lib::util::profile_encoder(&mail).unwrap();
    assert_eq!(expected, parsed);

    let wrong = lib::util::profile_encoder(&"foo@bar.com&role=admin");
    assert_eq!(wrong, None);
}